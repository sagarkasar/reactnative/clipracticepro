import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import HomeScreen from './Home';
import AboutScreen from './About';

const Tab = createMaterialTopTabNavigator();

const TopTabScreen = () => {
  return (
    <Tab.Navigator initialRouteName="Home">
      <Tab.Screen name="Home1" component={HomeScreen} />
      <Tab.Screen name="About1" component={AboutScreen} />
    </Tab.Navigator>
  );
};

export default TopTabScreen;

const styles = StyleSheet.create({});
