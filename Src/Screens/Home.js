import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.txtView}>
          <Text style={styles.mainTxt}>This is Sagar Kasar</Text>
          <Text style={styles.subtxt}>Welcome </Text>
          <Button
            title="Press Me"
            onPress={() => navigation.navigate('About')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  mainTxt: {
    color: '#6b9ff2',
    fontSize: 20,
  },
  subtxt: {
    color: '#1d61cf',
    fontSize: 18,
  },
  txtView: {
    backgroundColor: '#e6e0ac',
    alignItems: 'center',
  },
});

export default HomeScreen;
